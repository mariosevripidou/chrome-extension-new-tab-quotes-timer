(function(){

var $  = document.getElementById.bind(document);
var $$ = document.querySelectorAll.bind(document);

var App = function($el) {
  this.$el = $el;
  this.load();


  this.$el.addEventListener(
    'submit', this.submit.bind(this)
  );

  if (this.dob) {
    this.renderAgeLoop();
  } else {
    this.renderChoose();

  }

};

App.fn = App.prototype;

App.fn.load = function(){
  var value;
  if (value = localStorage.dob)
    this.dob = new Date(parseInt(value));
};

App.fn.save = function(){
  if (this.dob)
    localStorage.dob = this.dob.getTime();
};

App.fn.submit = function(e){
  e.preventDefault();

  var input = this.$$('input')[0];
  if ( !input.valueAsDate ) return;

  this.dob = input.valueAsDate;
  this.save();
  this.renderAgeLoop();
};

App.fn.renderChoose = function(){
  this.html(this.view('dob')());
};
App.fn.interval = 0;

App.fn.renderAgeLoop = function(){

  App.fn.interval = setInterval(this.renderAge.bind(this), 100);
};

App.fn.renderAge = function(){
  var now       = new Date
  var duration  = now - this.dob;
  var years     = duration / 31556900000;

  var majorMinor = years.toFixed(9).toString().split('.');

  requestAnimationFrame(function(){
    this.html(this.view('age')({
      year:         majorMinor[0],
      milliseconds: majorMinor[1]
    }));
  }.bind(this));
};

App.fn.$$ = function(sel){
  return this.$el.querySelectorAll(sel);
};

App.fn.html = function(html){
  this.$el.innerHTML = html;
};

App.fn.view = function(name){
  var $el = $(name + '-template');
  return Handlebars.compile($el.innerHTML);
};

App.fn.showSettings = function(){
  clearInterval(App.fn.interval);
  this.renderChoose();
};

App.fn.hideSettings = function(){
  if (this.dob) {
    this.renderAgeLoop();
  }
};

window.app = new App($('app'))

})();

//**********************************************     Date     *****************************************/
var now = new Date();
var days = new Array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
var months = new Array('January','February','March','April','May','June','July','August','September','October','November','December');
var date = ((now.getDate()<10) ? "0" : "")+ now.getDate();
function fourdigits(number)
{
	return (number < 1000) ? number + 1900 : number;
}

today = days[now.getDay()] + ", " + date + " " + months[now.getMonth()] + " " + (fourdigits(now.getYear()));
$("#date").hide().html(today).fadeIn('slow');

//**********************************************     Images     *****************************************/
  // Create an array of images that you'd like to use
    var images = [
        'img/001.jpg'
      , 'img/002.jpg'
      , 'img/003.jpg'
      , 'img/004.jpg'
      , 'img/005.jpg'
      , 'img/006.jpg'
      , 'img/007.jpg'
      , 'img/008.jpg'
      , 'img/009.jpg'
      , 'img/010.jpg'
      , 'img/011.jpg'
      , 'img/012.jpg'
      , 'img/013.jpg'
      , 'img/014.jpg'
      , 'img/015.jpg'
      , 'img/016.jpg'
      , 'img/017.jpg'
      , 'img/018.jpg'
      , 'img/019.jpg'
      , 'img/020.jpg'
      , 'img/021.jpg'
      , 'img/022.jpg'
      , 'img/023.jpg'
      , 'img/024.jpg'
      , 'img/025.jpg'
      , 'img/026.jpg'
      , 'img/027.jpg'
      , 'img/028.jpg'
      , 'img/029.jpg'
      , 'img/030.jpg'
      , 'img/031.jpg'
      , 'img/032.jpg'
	  , 'img/033.jpg'
      , 'img/034.jpg'
      , 'img/035.jpg'
      , 'img/036.jpg'
      , 'img/037.jpg'
      , 'img/038.jpg'
      , 'img/039.jpg'
      , 'img/040.jpg'
      , 'img/041.jpg'
      , 'img/042.jpg'
      , 'img/043.jpg'
      , 'img/044.jpg'
      , 'img/045.jpg'
      , 'img/046.jpg'
      , 'img/047.jpg'
      , 'img/048.jpg'
      , 'img/049.jpg'
      , 'img/050.jpg'
      , 'img/051.jpg'
      , 'img/052.jpg'
      , 'img/053.jpg'
      , 'img/054.jpg'
      , 'img/055.jpg'
      , 'img/056.jpg'
      , 'img/057.jpg'
      , 'img/058.jpg'
      , 'img/059.jpg'
      , 'img/060.jpg'
    ];

    // Get a random number between 0 and the number of images
    var randomNumber = Math.floor( Math.random() * images.length );
    // Use the random number to load a random image
    $.backstretch("assets/"+images[randomNumber]);

//**********************************************     Timer     *****************************************/
     $('.timer').countTo();

//**********************************************     Click Events     *********************************/
    //add click events
    $("#settings-btn-show").click(function() {app.showSettings();});

