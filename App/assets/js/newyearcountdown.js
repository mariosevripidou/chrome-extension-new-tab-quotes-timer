	var newyear, today, bday, diff, days;
	newyear = [1,1]; // 6th of February
	today = new Date();
	bday = new Date(today.getFullYear(),newyear[1]-1,newyear[0]);
	if( today.getTime() > bday.getTime()) {
	    bday.setFullYear(bday.getFullYear()+1);
	}
	diff = bday.getTime()-today.getTime();
	days = Math.floor(diff/(1000*60*60*24));
	$("#days-count").attr("data-to",days);
